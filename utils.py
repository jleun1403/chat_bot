# -*- coding: utf-8 -*-
import csv
import json
from requests.models import PreparedRequest
from crwal_data import crawl

class VariableObj:
    def __init__(self):
        self.isSearched = False
        self.start = 0

    def set_searched(self, true_or_false):
        self.isSearched = true_or_false

    def get_searched(self):
        return self.isSearched

    def set_start(self, index):
        self.start = index

    def get_start(self):
        return self.start


# #JSON 파일을 읽고 문자열을 딕셔너리로 변환합니다.
def create_dict(filename):
    with open(filename) as file:
        json_string = file.read()
        return json.loads(json_string, encoding='utf-8')

# 수집한 카테고리를 csv파일로부터 읽어온다.
def read_category(SEACHED):
    # get_category()
    message = ""
    with open('data/category.csv', encoding='utf-8') as file:
        reader = csv.reader(file, delimiter=",")
        for row in reader:
            count = 15 if SEACHED == False else (len(row) - 1)
            for contents ,j in zip(row, range(count)):
                if j % 8 == 0:
                    message +="\n"
                else:
                    message += "`"+contents+"` "
    return message
import threading
# 입력된 text로 검색을 하고, 크롤링 된거로 리스트를 만들고 해먹지수를 기준으로 반환
def search(text):
    text = text.replace('-s ','')
    req = PreparedRequest()
    first_url = 'http://haemukja.com/recipes?utf8=%E2%9C%93&sort=rlv&'
    txt = text.replace('/',"+")
    params = {'name':txt}
    req.prepare_url(first_url, params)
    # URL에 검색내용을 추가해서 크롤링한다
    # result = threading.Thread(target = crawl, args = req.url)
    # result.start()
    result = crawl(req.url)
    print(result)
    # 크롤링으로 긁어오고, 정렬한 탑3 추천목록은 스트링 형태로 반환된다.
    return result
