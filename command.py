# -*- coding: utf-8 -*-
from utils import read_category

def help_guide():
    text1 ="*사용 방식*\n ```카테고리와 재료, 그리고 요리 이름으로도 검색할 수 있습니다```\n"
    text2 = "*명령어 키워드*\n" \
           "```1) '-help' : 도움말을 보여줍니다.\n" \
            "2) '-category' : 전체 카테고리를 보여줍니다.\n" \
            "3) '-s' : Search의 약자로, @BOT (category_name or materials) 이러한 방식으로 검색에 사용합니다.\n" \
            "4) '-next' : 다음 추천목록을 확인합니다. 추천목록이 검색된 상태에서만 가능합니다.\n" \
            "5) '-prev' : 이전 추천목록을 확인합니다. 추천목록이 검색된 상태에서만 가능합니다.\n"\
            "6) '-q' : 종료 명령어 입니다.\n"\
           "```"
    return text1 + text2


def print_category(display, SEARCHED):
    display[3]['text']['text'] = read_category(SEARCHED)


def print_recipe(display, word, material, image, index):
    for i, j in zip(range(index, index+3),range(5,8)):
        display[j]['text']['text'] = word[i] + "```" + ", ".join(material[i]) + "```"
        display[j]['accessory']['image_url'] = image[i]
        #5,6,7