# -*- coding: utf-8 -*-
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes.blocks import *
from utils import search, create_dict, VariableObj, read_category
from command import help_guide, print_category, print_recipe
from config import SLACK_TOKEN, SLACK_SIGNING_SECRET
import time
vars = VariableObj()

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)
import threading

# 챗봇이 멘션을 받았을 경우 # 명령어에 따라 멘션을 다르게 해야해
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    text = text.replace('<@UKYP73Z9Q> ', '')
    case = input_command(text, channel)
    print(case)
    SEARCHED, index = vars.get_searched(), vars.get_start()
    #케이스값 주고, 1h2c3s4n5p6q
    if SEARCHED == False : #1
        if case in [0,1]:
            home_display = create_dict('data/home.json')
            print_category(home_display, SEARCHED)
            slack_web_client.chat_postMessage(
                channel = channel,
                blocks = extract_json(home_display)
            )
    else : #3,4,5
        if case in [3,4,5]:
            # 주어진 텍스트로 검색한 결과 중 탑3를 추출합니다.
            word_message, image_message, material_message = search(text)
            # result.json 블록을 갱신합니다.
            res_display = create_dict('data/result.json')
            print_category(res_display, SEARCHED)
            print_recipe(res_display, word_message, material_message, image_message, index)
            slack_web_client.chat_postMessage(
                channel=channel,
                blocks= extract_json(res_display)
            )


def input_command(command, channel):
    case = 0
    default_query = ""#Please Enter the mention like `@BOT (category_name or materials)` "
    query = None

    if command.startswith("-"):
        if command == "-help":
            case = 1
            vars.set_searched(False)
            query = help_guide()
        if command == "-category":
            case = 2
            vars.set_searched(False)
            home_display = create_dict('data/home.json')
            home_display[3]['text']['text'] = read_category(True)
            slack_web_client.chat_postMessage(
                channel=channel,
                blocks=extract_json(home_display)
            )
            query = ""
        if command.startswith("-s"):
            case = 3
            vars.set_searched(True)
            vars.set_start(0)
            query = ""
            return case
        if command == "-next":
            case = 4
            if vars.get_searched():
                vars.set_start((vars.get_start()+3)%12)
            #     query =""
            # else :
            #     query = "추천 레시피를 보고 싶으시다면 검색을 먼저 부탁드립니다.\n"
            return case
        if command == "-prev":
            case = 5
            if vars.get_searched():
                vars.set_start((vars.get_start() - 3) % 12)
            #     query = ""
            # else :
            #     query = "추천 레시피를 보고 싶으시다면 검색을 먼저 부탁드립니다.\n"
            return case
        if command == "-q":
            case = 6
            vars.set_searched(False)
            query = "종료합니다."

        if command == None:
            vars.set_searched(False)
            query = "유효하지 않은 명령어입니다. -help를 입력해 명령어를 확인해 보세요.\n"
    else:
        query = "유효하지 않은 명령어입니다. -help를 입력해 명령어를 확인해 보세요.\n"

    slack_web_client.chat_postMessage(
        channel=channel,
        text=query or default_query
    )
    return case

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready! no....</h1>"

if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)