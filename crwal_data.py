# -*- coding: utf-8 -*-

import urllib.request
from bs4 import BeautifulSoup

# 카테고리를 수집한다.
def get_category():
    # 카테고리를 수집한다. -> 메세지로 입력 가능한 형태를 보여줄 것 # 수집완료
    url = "http://haemukja.com/recipes?utf8=%E2%9C%93"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    res = []
    for category in soup.find_all("div", class_="finder"):
         for name in category.find_all("ul", class_="lst_check"):
             for title in name.find_all("label"):
                 res.append(title.get_text().strip())
    categorySet = set(res)
    f = open("data/category.csv","w", encoding="utf-8")
    for i in categorySet:
        f.write(i+",")
    f.close()

# 제목, URL, 이미지링크, 점수를 가져오는 크롤링
def crawl(url):
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    lists = []  # 전체를 담을 리스트
    name_list, judge_list, url_list, image_list = [], [], [], []  # 요리명, 요리점수, URL주소, 이미지를 가져온다

    for item in soup.find_all("ul", class_="lst_recipe"):
        for im in item.find_all("a", class_="call_recipe thmb"):  # URL, 이미지
            url_list.append("https://haemukja.com" + im["href"])
            for image in im.find_all("img"):
                image_list.append(image.get("src"))
        for judge in item.find_all("span", class_="judge"):  # 점수
            judge_list.append(judge.find("strong").get_text())
        for name in item.find_all("p"):  # 요리명
            name_list.append(name.find("strong").get_text().strip())

    for i in range(len(name_list)):
        lists.append([name_list[i], judge_list[i], url_list[i], image_list[i]])

    # 해먹점수에 따른 정렬
    lists = sorted(lists, key=lambda x: x[1], reverse=True)
    return assemble_data(lists)

# 재료정보 수집하기
def crawl_material(url):
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    material_list = []
    for item in soup.find_all("ul", class_="lst_ingrd"):
        for i, j in zip(item.find_all("span"),item.find_all("em")):
            material_list.append((i.get_text()+"("+j.get_text()+")"))
    return material_list

# 해당 링크에서 새로 재료 정보 크롤링
def assemble_data(lists):
    word_list, image_url_list, material_list = [], [], [] # [제목,점수,URL], [이미지주소], [재료정보]
    for list in lists:
        word_list.append("*"+str(list[0])+"*\n :star: "+str(list[1])+" URL: "+str(list[2])+"\n")
        image_url_list.append(str(list[3]))
        material_list.append(crawl_material(list[2])) # URL을 통해 접속하여 새롭게 크롤링한다
    return word_list, image_url_list, material_list


